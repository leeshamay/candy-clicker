﻿namespace WindowsFormsApplication1
{
    partial class UserInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MachFact = new System.Windows.Forms.TabPage();
            this.SpecialItems = new System.Windows.Forms.TabPage();
            this.Items = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.MachTimer = new System.Windows.Forms.Timer(this.components);
            this.FactoryTimer = new System.Windows.Forms.Timer(this.components);
            this.LabelUpdate = new System.Windows.Forms.Timer(this.components);
            this.ReqCheck = new System.Windows.Forms.Timer(this.components);
            this.ThisToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MachFact
            // 
            this.MachFact.AutoScroll = true;
            this.MachFact.Location = new System.Drawing.Point(4, 25);
            this.MachFact.Name = "MachFact";
            this.MachFact.Size = new System.Drawing.Size(662, 241);
            this.MachFact.TabIndex = 5;
            this.MachFact.Text = "Machines and Factories";
            this.MachFact.UseVisualStyleBackColor = true;
            // 
            // SpecialItems
            // 
            this.SpecialItems.AutoScroll = true;
            this.SpecialItems.Location = new System.Drawing.Point(4, 25);
            this.SpecialItems.Name = "SpecialItems";
            this.SpecialItems.Padding = new System.Windows.Forms.Padding(3);
            this.SpecialItems.Size = new System.Drawing.Size(662, 241);
            this.SpecialItems.TabIndex = 3;
            this.SpecialItems.Text = "Special Items";
            this.SpecialItems.UseVisualStyleBackColor = true;
            // 
            // Items
            // 
            this.Items.AutoScroll = true;
            this.Items.Location = new System.Drawing.Point(4, 25);
            this.Items.Name = "Items";
            this.Items.Padding = new System.Windows.Forms.Padding(3);
            this.Items.Size = new System.Drawing.Size(662, 241);
            this.Items.TabIndex = 2;
            this.Items.Text = "Items";
            this.Items.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Items);
            this.tabControl1.Controls.Add(this.SpecialItems);
            this.tabControl1.Controls.Add(this.MachFact);
            this.tabControl1.Location = new System.Drawing.Point(8, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(670, 270);
            this.tabControl1.TabIndex = 131;
            // 
            // FactoryTimer
            // 
            this.FactoryTimer.Enabled = true;
            this.FactoryTimer.Interval = 1000;
            this.FactoryTimer.Tick += new System.EventHandler(this.FactoryTimers_Tick);
            // 
            // LabelUpdate
            // 
            this.LabelUpdate.Enabled = true;
            this.LabelUpdate.Tick += new System.EventHandler(this.LabelUpdate_Tick);
            // 
            // ReqCheck
            // 
            this.ReqCheck.Enabled = true;
            this.ReqCheck.Tick += new System.EventHandler(this.ReqCheck_Timer);
            // 
            // UserInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(686, 279);
            this.ControlBox = false;
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "UserInventory";
            this.Text = "Inventory";
            this.Load += new System.EventHandler(this.UserInventory_Load);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage MachFact;
        private System.Windows.Forms.TabPage SpecialItems;
        private System.Windows.Forms.TabPage Items;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Timer MachTimer;
        private System.Windows.Forms.Timer FactoryTimer;
        private System.Windows.Forms.Timer LabelUpdate;
        private System.Windows.Forms.Timer ReqCheck;
        private System.Windows.Forms.ToolTip ThisToolTip;

    }
}