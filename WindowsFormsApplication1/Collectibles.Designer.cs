﻿namespace WindowsFormsApplication1
{
    partial class Collectibles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SculptureTimer = new System.Windows.Forms.Timer(this.components);
            this.tabPageGolden = new System.Windows.Forms.TabPage();
            this.chkGS = new System.Windows.Forms.CheckBox();
            this.tabPageChocoStat = new System.Windows.Forms.TabPage();
            this.chkFlower = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPageSculptures = new System.Windows.Forms.TabPage();
            this.chkSculptures = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.labelSwan = new System.Windows.Forms.Label();
            this.btnDinosaur = new System.Windows.Forms.Button();
            this.lblDog = new System.Windows.Forms.Label();
            this.lblCat = new System.Windows.Forms.Label();
            this.btnTrophy = new System.Windows.Forms.Button();
            this.lblMonkey = new System.Windows.Forms.Label();
            this.labelCat = new System.Windows.Forms.Label();
            this.lblTrophy = new System.Windows.Forms.Label();
            this.btnCat = new System.Windows.Forms.Button();
            this.lblSwan = new System.Windows.Forms.Label();
            this.labelTrophy = new System.Windows.Forms.Label();
            this.lblDino = new System.Windows.Forms.Label();
            this.btnDog = new System.Windows.Forms.Button();
            this.labelDog = new System.Windows.Forms.Label();
            this.btnMonkey = new System.Windows.Forms.Button();
            this.labelMonkey = new System.Windows.Forms.Label();
            this.labelDinosaur = new System.Windows.Forms.Label();
            this.btnSwan = new System.Windows.Forms.Button();
            this.tabPageGardening = new System.Windows.Forms.TabPage();
            this.chkGT = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.collectiblesTab = new System.Windows.Forms.TabControl();
            this.GTTimer = new System.Windows.Forms.Timer(this.components);
            this.FlowerTimer = new System.Windows.Forms.Timer(this.components);
            this.GSTimer = new System.Windows.Forms.Timer(this.components);
            this.Updater = new System.Windows.Forms.Timer(this.components);
            this.tabPageGolden.SuspendLayout();
            this.tabPageChocoStat.SuspendLayout();
            this.tabPageSculptures.SuspendLayout();
            this.tabPageGardening.SuspendLayout();
            this.collectiblesTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // SculptureTimer
            // 
            this.SculptureTimer.Tick += new System.EventHandler(this.SculptureTimer_Tick);
            // 
            // tabPageGolden
            // 
            this.tabPageGolden.BackColor = System.Drawing.Color.White;
            this.tabPageGolden.Controls.Add(this.chkGS);
            this.tabPageGolden.Location = new System.Drawing.Point(4, 32);
            this.tabPageGolden.Name = "tabPageGolden";
            this.tabPageGolden.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGolden.Size = new System.Drawing.Size(663, 395);
            this.tabPageGolden.TabIndex = 2;
            this.tabPageGolden.Text = "Golden Stuff";
            // 
            // chkGS
            // 
            this.chkGS.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGS.AutoSize = true;
            this.chkGS.Enabled = false;
            this.chkGS.Location = new System.Drawing.Point(579, 339);
            this.chkGS.Name = "chkGS";
            this.chkGS.Size = new System.Drawing.Size(66, 34);
            this.chkGS.TabIndex = 59;
            this.chkGS.Text = "Enable";
            this.chkGS.UseVisualStyleBackColor = true;
            this.chkGS.Click += new System.EventHandler(this.CheckIfEnabled);
            // 
            // tabPageChocoStat
            // 
            this.tabPageChocoStat.BackColor = System.Drawing.Color.White;
            this.tabPageChocoStat.Controls.Add(this.chkFlower);
            this.tabPageChocoStat.Controls.Add(this.label21);
            this.tabPageChocoStat.Controls.Add(this.label20);
            this.tabPageChocoStat.Controls.Add(this.label19);
            this.tabPageChocoStat.Controls.Add(this.label18);
            this.tabPageChocoStat.Controls.Add(this.label17);
            this.tabPageChocoStat.Controls.Add(this.label16);
            this.tabPageChocoStat.Location = new System.Drawing.Point(4, 32);
            this.tabPageChocoStat.Name = "tabPageChocoStat";
            this.tabPageChocoStat.Size = new System.Drawing.Size(663, 395);
            this.tabPageChocoStat.TabIndex = 3;
            this.tabPageChocoStat.Text = "Flowers";
            // 
            // chkFlower
            // 
            this.chkFlower.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkFlower.AutoSize = true;
            this.chkFlower.Enabled = false;
            this.chkFlower.Location = new System.Drawing.Point(579, 339);
            this.chkFlower.Name = "chkFlower";
            this.chkFlower.Size = new System.Drawing.Size(66, 34);
            this.chkFlower.TabIndex = 59;
            this.chkFlower.Text = "Enable";
            this.chkFlower.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(472, 166);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 17);
            this.label21.TabIndex = 43;
            this.label21.Text = "Mint Patty";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(465, 139);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 17);
            this.label20.TabIndex = 42;
            this.label20.Text = "Buttercream";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(499, 111);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(38, 17);
            this.label19.TabIndex = 41;
            this.label19.Text = "Fudge";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(498, 83);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 17);
            this.label18.TabIndex = 40;
            this.label18.Text = "Toffee";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(494, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 17);
            this.label17.TabIndex = 39;
            this.label17.Text = "Truffle";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(496, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 17);
            this.label16.TabIndex = 38;
            this.label16.Text = "Brittle";
            // 
            // tabPageSculptures
            // 
            this.tabPageSculptures.BackColor = System.Drawing.Color.White;
            this.tabPageSculptures.Controls.Add(this.chkSculptures);
            this.tabPageSculptures.Controls.Add(this.label23);
            this.tabPageSculptures.Controls.Add(this.label29);
            this.tabPageSculptures.Controls.Add(this.label30);
            this.tabPageSculptures.Controls.Add(this.label22);
            this.tabPageSculptures.Controls.Add(this.label28);
            this.tabPageSculptures.Controls.Add(this.label31);
            this.tabPageSculptures.Controls.Add(this.label24);
            this.tabPageSculptures.Controls.Add(this.label25);
            this.tabPageSculptures.Controls.Add(this.label26);
            this.tabPageSculptures.Controls.Add(this.label27);
            this.tabPageSculptures.Controls.Add(this.labelSwan);
            this.tabPageSculptures.Controls.Add(this.btnDinosaur);
            this.tabPageSculptures.Controls.Add(this.lblDog);
            this.tabPageSculptures.Controls.Add(this.lblCat);
            this.tabPageSculptures.Controls.Add(this.btnTrophy);
            this.tabPageSculptures.Controls.Add(this.lblMonkey);
            this.tabPageSculptures.Controls.Add(this.labelCat);
            this.tabPageSculptures.Controls.Add(this.lblTrophy);
            this.tabPageSculptures.Controls.Add(this.btnCat);
            this.tabPageSculptures.Controls.Add(this.lblSwan);
            this.tabPageSculptures.Controls.Add(this.labelTrophy);
            this.tabPageSculptures.Controls.Add(this.lblDino);
            this.tabPageSculptures.Controls.Add(this.btnDog);
            this.tabPageSculptures.Controls.Add(this.labelDog);
            this.tabPageSculptures.Controls.Add(this.btnMonkey);
            this.tabPageSculptures.Controls.Add(this.labelMonkey);
            this.tabPageSculptures.Controls.Add(this.labelDinosaur);
            this.tabPageSculptures.Controls.Add(this.btnSwan);
            this.tabPageSculptures.Location = new System.Drawing.Point(4, 32);
            this.tabPageSculptures.Name = "tabPageSculptures";
            this.tabPageSculptures.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSculptures.Size = new System.Drawing.Size(663, 395);
            this.tabPageSculptures.TabIndex = 0;
            this.tabPageSculptures.Text = "Sculptures";
            // 
            // chkSculptures
            // 
            this.chkSculptures.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkSculptures.AutoSize = true;
            this.chkSculptures.Enabled = false;
            this.chkSculptures.Location = new System.Drawing.Point(579, 339);
            this.chkSculptures.Name = "chkSculptures";
            this.chkSculptures.Size = new System.Drawing.Size(66, 34);
            this.chkSculptures.TabIndex = 58;
            this.chkSculptures.Text = "Enable";
            this.chkSculptures.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(471, 185);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 17);
            this.label23.TabIndex = 57;
            this.label23.Text = "Sugarcubes";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(447, 163);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(87, 17);
            this.label29.TabIndex = 56;
            this.label29.Text = "Chocolate Chips";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(495, 140);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 17);
            this.label30.TabIndex = 55;
            this.label30.Text = "Candy";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(471, 269);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 17);
            this.label22.TabIndex = 54;
            this.label22.Text = "Sugarcubes";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(447, 247);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(87, 17);
            this.label28.TabIndex = 53;
            this.label28.Text = "Chocolate Chips";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(495, 224);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 17);
            this.label31.TabIndex = 52;
            this.label31.Text = "Candy";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(473, 110);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 17);
            this.label24.TabIndex = 47;
            this.label24.Text = "Sugarcubes";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(447, 82);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 17);
            this.label25.TabIndex = 46;
            this.label25.Text = "Chocolate Chips";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(468, 54);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 17);
            this.label26.TabIndex = 45;
            this.label26.Text = "Candy Dust";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(468, 26);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 17);
            this.label27.TabIndex = 44;
            this.label27.Text = "Candy Dust";
            // 
            // labelSwan
            // 
            this.labelSwan.AutoSize = true;
            this.labelSwan.Location = new System.Drawing.Point(20, 23);
            this.labelSwan.Name = "labelSwan";
            this.labelSwan.Size = new System.Drawing.Size(48, 24);
            this.labelSwan.TabIndex = 0;
            this.labelSwan.Text = "Swan";
            // 
            // btnDinosaur
            // 
            this.btnDinosaur.Enabled = false;
            this.btnDinosaur.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDinosaur.Location = new System.Drawing.Point(542, 135);
            this.btnDinosaur.Name = "btnDinosaur";
            this.btnDinosaur.Size = new System.Drawing.Size(75, 23);
            this.btnDinosaur.TabIndex = 18;
            this.btnDinosaur.Text = "Collect";
            this.btnDinosaur.UseVisualStyleBackColor = true;
            this.btnDinosaur.Click += new System.EventHandler(this.btnDinosaur_Click);
            // 
            // lblDog
            // 
            this.lblDog.AutoSize = true;
            this.lblDog.Location = new System.Drawing.Point(115, 79);
            this.lblDog.Name = "lblDog";
            this.lblDog.Size = new System.Drawing.Size(20, 24);
            this.lblDog.TabIndex = 9;
            this.lblDog.Text = "0";
            // 
            // lblCat
            // 
            this.lblCat.AutoSize = true;
            this.lblCat.Location = new System.Drawing.Point(115, 107);
            this.lblCat.Name = "lblCat";
            this.lblCat.Size = new System.Drawing.Size(20, 24);
            this.lblCat.TabIndex = 10;
            this.lblCat.Text = "0";
            // 
            // btnTrophy
            // 
            this.btnTrophy.Enabled = false;
            this.btnTrophy.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrophy.Location = new System.Drawing.Point(542, 224);
            this.btnTrophy.Name = "btnTrophy";
            this.btnTrophy.Size = new System.Drawing.Size(75, 23);
            this.btnTrophy.TabIndex = 17;
            this.btnTrophy.Text = "Collect";
            this.btnTrophy.UseVisualStyleBackColor = true;
            this.btnTrophy.Click += new System.EventHandler(this.btnTrophy_Click);
            // 
            // lblMonkey
            // 
            this.lblMonkey.AutoSize = true;
            this.lblMonkey.Location = new System.Drawing.Point(115, 51);
            this.lblMonkey.Name = "lblMonkey";
            this.lblMonkey.Size = new System.Drawing.Size(20, 24);
            this.lblMonkey.TabIndex = 8;
            this.lblMonkey.Text = "0";
            // 
            // labelCat
            // 
            this.labelCat.AutoSize = true;
            this.labelCat.Location = new System.Drawing.Point(20, 107);
            this.labelCat.Name = "labelCat";
            this.labelCat.Size = new System.Drawing.Size(35, 24);
            this.labelCat.TabIndex = 3;
            this.labelCat.Text = "Cat";
            // 
            // lblTrophy
            // 
            this.lblTrophy.AutoSize = true;
            this.lblTrophy.Location = new System.Drawing.Point(115, 219);
            this.lblTrophy.Name = "lblTrophy";
            this.lblTrophy.Size = new System.Drawing.Size(20, 24);
            this.lblTrophy.TabIndex = 11;
            this.lblTrophy.Text = "0";
            // 
            // btnCat
            // 
            this.btnCat.Enabled = false;
            this.btnCat.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCat.Location = new System.Drawing.Point(542, 107);
            this.btnCat.Name = "btnCat";
            this.btnCat.Size = new System.Drawing.Size(75, 23);
            this.btnCat.TabIndex = 16;
            this.btnCat.Text = "Collect";
            this.btnCat.UseVisualStyleBackColor = true;
            this.btnCat.Click += new System.EventHandler(this.btnCat_Click);
            // 
            // lblSwan
            // 
            this.lblSwan.AutoSize = true;
            this.lblSwan.Location = new System.Drawing.Point(115, 23);
            this.lblSwan.Name = "lblSwan";
            this.lblSwan.Size = new System.Drawing.Size(20, 24);
            this.lblSwan.TabIndex = 7;
            this.lblSwan.Text = "0";
            // 
            // labelTrophy
            // 
            this.labelTrophy.AutoSize = true;
            this.labelTrophy.Location = new System.Drawing.Point(20, 219);
            this.labelTrophy.Name = "labelTrophy";
            this.labelTrophy.Size = new System.Drawing.Size(60, 24);
            this.labelTrophy.TabIndex = 4;
            this.labelTrophy.Text = "Trophy";
            // 
            // lblDino
            // 
            this.lblDino.AutoSize = true;
            this.lblDino.Location = new System.Drawing.Point(115, 135);
            this.lblDino.Name = "lblDino";
            this.lblDino.Size = new System.Drawing.Size(20, 24);
            this.lblDino.TabIndex = 12;
            this.lblDino.Text = "0";
            // 
            // btnDog
            // 
            this.btnDog.Enabled = false;
            this.btnDog.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDog.Location = new System.Drawing.Point(542, 79);
            this.btnDog.Name = "btnDog";
            this.btnDog.Size = new System.Drawing.Size(75, 23);
            this.btnDog.TabIndex = 15;
            this.btnDog.Text = "Collect";
            this.btnDog.UseVisualStyleBackColor = true;
            this.btnDog.Click += new System.EventHandler(this.btnDog_Click);
            // 
            // labelDog
            // 
            this.labelDog.AutoSize = true;
            this.labelDog.Location = new System.Drawing.Point(20, 79);
            this.labelDog.Name = "labelDog";
            this.labelDog.Size = new System.Drawing.Size(37, 24);
            this.labelDog.TabIndex = 2;
            this.labelDog.Text = "Dog";
            // 
            // btnMonkey
            // 
            this.btnMonkey.Enabled = false;
            this.btnMonkey.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonkey.Location = new System.Drawing.Point(542, 51);
            this.btnMonkey.Name = "btnMonkey";
            this.btnMonkey.Size = new System.Drawing.Size(75, 23);
            this.btnMonkey.TabIndex = 14;
            this.btnMonkey.Text = "Collect";
            this.btnMonkey.UseVisualStyleBackColor = true;
            this.btnMonkey.Click += new System.EventHandler(this.btnMonkey_Click);
            // 
            // labelMonkey
            // 
            this.labelMonkey.AutoSize = true;
            this.labelMonkey.Location = new System.Drawing.Point(20, 51);
            this.labelMonkey.Name = "labelMonkey";
            this.labelMonkey.Size = new System.Drawing.Size(63, 24);
            this.labelMonkey.TabIndex = 1;
            this.labelMonkey.Text = "Monkey";
            // 
            // labelDinosaur
            // 
            this.labelDinosaur.AutoSize = true;
            this.labelDinosaur.CausesValidation = false;
            this.labelDinosaur.Location = new System.Drawing.Point(20, 135);
            this.labelDinosaur.Name = "labelDinosaur";
            this.labelDinosaur.Size = new System.Drawing.Size(72, 24);
            this.labelDinosaur.TabIndex = 5;
            this.labelDinosaur.Text = "Dinosaur";
            // 
            // btnSwan
            // 
            this.btnSwan.Enabled = false;
            this.btnSwan.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSwan.Location = new System.Drawing.Point(542, 23);
            this.btnSwan.Name = "btnSwan";
            this.btnSwan.Size = new System.Drawing.Size(75, 23);
            this.btnSwan.TabIndex = 8;
            this.btnSwan.Text = "Collect";
            this.btnSwan.UseVisualStyleBackColor = true;
            this.btnSwan.Click += new System.EventHandler(this.btnSwan_Click);
            // 
            // tabPageGardening
            // 
            this.tabPageGardening.AutoScroll = true;
            this.tabPageGardening.BackColor = System.Drawing.Color.White;
            this.tabPageGardening.Controls.Add(this.chkGT);
            this.tabPageGardening.Controls.Add(this.label15);
            this.tabPageGardening.Controls.Add(this.label14);
            this.tabPageGardening.Controls.Add(this.label13);
            this.tabPageGardening.Controls.Add(this.label8);
            this.tabPageGardening.Controls.Add(this.label6);
            this.tabPageGardening.Controls.Add(this.label5);
            this.tabPageGardening.Controls.Add(this.label4);
            this.tabPageGardening.Controls.Add(this.label3);
            this.tabPageGardening.Controls.Add(this.label2);
            this.tabPageGardening.Location = new System.Drawing.Point(4, 32);
            this.tabPageGardening.Name = "tabPageGardening";
            this.tabPageGardening.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGardening.Size = new System.Drawing.Size(663, 395);
            this.tabPageGardening.TabIndex = 1;
            this.tabPageGardening.Text = "Garden Tools";
            // 
            // chkGT
            // 
            this.chkGT.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGT.AutoSize = true;
            this.chkGT.Enabled = false;
            this.chkGT.Location = new System.Drawing.Point(579, 339);
            this.chkGT.Name = "chkGT";
            this.chkGT.Size = new System.Drawing.Size(66, 34);
            this.chkGT.TabIndex = 59;
            this.chkGT.Text = "Enable";
            this.chkGT.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(501, 247);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 17);
            this.label15.TabIndex = 56;
            this.label15.Text = "Taffy";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(451, 218);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 17);
            this.label14.TabIndex = 55;
            this.label14.Text = "Gummy Candy";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(466, 192);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 17);
            this.label13.TabIndex = 54;
            this.label13.Text = "Candy Corn";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(470, 165);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 17);
            this.label8.TabIndex = 53;
            this.label8.Text = "Jawbreaker";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(504, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 17);
            this.label6.TabIndex = 52;
            this.label6.Text = "Mint";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(470, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 51;
            this.label5.Text = "Candy Ring";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(489, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 50;
            this.label4.Text = "Lollipop";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(504, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 17);
            this.label3.TabIndex = 49;
            this.label3.Text = "Gum";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(472, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 48;
            this.label2.Text = "Pop Candy";
            // 
            // collectiblesTab
            // 
            this.collectiblesTab.Controls.Add(this.tabPageGardening);
            this.collectiblesTab.Controls.Add(this.tabPageSculptures);
            this.collectiblesTab.Controls.Add(this.tabPageChocoStat);
            this.collectiblesTab.Controls.Add(this.tabPageGolden);
            this.collectiblesTab.Font = new System.Drawing.Font("Segoe Print", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.collectiblesTab.Location = new System.Drawing.Point(13, 13);
            this.collectiblesTab.Name = "collectiblesTab";
            this.collectiblesTab.SelectedIndex = 0;
            this.collectiblesTab.Size = new System.Drawing.Size(671, 431);
            this.collectiblesTab.TabIndex = 8;
            // 
            // GTTimer
            // 
            this.GTTimer.Tick += new System.EventHandler(this.GTTimer_Tick);
            // 
            // FlowerTimer
            // 
            this.FlowerTimer.Tick += new System.EventHandler(this.FlowerTimer_Tick);
            // 
            // GSTimer
            // 
            this.GSTimer.Tick += new System.EventHandler(this.GSTimer_Tick);
            // 
            // Updater
            // 
            this.Updater.Enabled = true;
            this.Updater.Tick += new System.EventHandler(this.Updater_Tick);
            // 
            // Collectibles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 456);
            this.ControlBox = false;
            this.Controls.Add(this.collectiblesTab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Collectibles";
            this.Text = "Collectibles";
            this.Load += new System.EventHandler(this.Collectibles_Load);
            this.tabPageGolden.ResumeLayout(false);
            this.tabPageGolden.PerformLayout();
            this.tabPageChocoStat.ResumeLayout(false);
            this.tabPageChocoStat.PerformLayout();
            this.tabPageSculptures.ResumeLayout(false);
            this.tabPageSculptures.PerformLayout();
            this.tabPageGardening.ResumeLayout(false);
            this.tabPageGardening.PerformLayout();
            this.collectiblesTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer SculptureTimer;
        private System.Windows.Forms.TabPage tabPageGolden;
        private System.Windows.Forms.TabPage tabPageChocoStat;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabPageSculptures;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnDinosaur;
        private System.Windows.Forms.Button btnTrophy;
        private System.Windows.Forms.Button btnCat;
        private System.Windows.Forms.Button btnDog;
        private System.Windows.Forms.Button btnMonkey;
        private System.Windows.Forms.Button btnSwan;
        private System.Windows.Forms.TabPage tabPageGardening;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl collectiblesTab;
        private System.Windows.Forms.CheckBox chkSculptures;
        private System.Windows.Forms.CheckBox chkGT;
        private System.Windows.Forms.CheckBox chkGS;
        private System.Windows.Forms.Timer GTTimer;
        private System.Windows.Forms.Timer FlowerTimer;
        private System.Windows.Forms.Timer GSTimer;
        public System.Windows.Forms.CheckBox chkFlower;
        private System.Windows.Forms.Label labelSwan;
        private System.Windows.Forms.Label lblDog;
        private System.Windows.Forms.Label lblCat;
        private System.Windows.Forms.Label lblMonkey;
        private System.Windows.Forms.Label labelCat;
        private System.Windows.Forms.Label lblTrophy;
        private System.Windows.Forms.Label lblSwan;
        private System.Windows.Forms.Label labelTrophy;
        private System.Windows.Forms.Label lblDino;
        private System.Windows.Forms.Label labelDog;
        private System.Windows.Forms.Label labelMonkey;
        private System.Windows.Forms.Label labelDinosaur;
        private System.Windows.Forms.Timer Updater;




    }
}