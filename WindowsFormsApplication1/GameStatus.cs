﻿using Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace WindowsFormsApplication1
{
    public class GameStatus
    {
        public Dictionary<string, Item> AllItemsInTheGame = new Dictionary<string, Item>();
        public Dictionary<string, decimal> PlayerInventory = new Dictionary<string, decimal>();
        public Dictionary<string, Trade> AllTrades = new Dictionary<string, Trade>();
        public List<Trade> CurrentTrades = new List<Trade>();



        public GameStatus(JObject jRef)
        {
            List<Item> itemList = jRef.ParsePropertyToListWithDelegate<Item>("Items", Item.ParseJToken);
            for (int i = 0; i < itemList.Count; i++)
            {
                AllItemsInTheGame.Add(itemList[i].ID, itemList[i]);
            }
            foreach (KeyValuePair<string, Item> kvp in AllItemsInTheGame)
            {
                if (!PlayerInventory.ContainsKey(kvp.Key))
                    PlayerInventory.Add(kvp.Key, 0);
            }
            List<Engine.Trade> questList = jRef.ParsePropertyToListWithDelegate<Engine.Trade>("Quests", Engine.Trade.ParseToken);
            for (int i = 0; i < questList.Count; i++)
            {
                AllTrades.Add(questList[i].ID, questList[i]);
            }
            OnItemQtyChanged += _ItemsChanged_EventHandler_Internal;
            OnTradeReady += _TradeAvailable_EventHandler_Internal;
        }

        #region Inventory Update Event
        public delegate void ItemQtyChangedDelegate(string sKey, decimal dQty);
        public event ItemQtyChangedDelegate OnItemQtyChanged;
        private void FireInventoryUpdatedEvent(string sInventoryKey, decimal dNewInventoryQty)
        {
            if (OnItemQtyChanged != null)//If our event has assigned handlers
            {
                Delegate[] invokeDelegateArray = OnItemQtyChanged.GetInvocationList();//Grab all the delgates to the event handlers
                for (int i = 0; i < invokeDelegateArray.Length; i++)//Loop through them
                {
                    invokeDelegateArray[i].DynamicInvoke(sInventoryKey, dNewInventoryQty);//Call them and pass in the parameters.
                }
            }
        }

        /// <summary>
        /// Internal event handler that subscribes to ItemQty changes and uses that to evaluate if any Quests have been completed.
        /// </summary>
        /// <param name="sKey">Key (ID in string format) of the item that's been updated.</param>
        /// <param name="dQty">Quantity in inventory of the item.</param>
        private void _ItemsChanged_EventHandler_Internal(string sKey, decimal dQty)
        {
            Engine.Trade[] questsUsingThisItem = AllTrades.Where(x => x.Value.RequiredItems.ContainsKey(sKey)).Select(x => x.Value).ToArray();//this line selects all the quests that require a specific item ID (sKey) and puts them in an array so we can loop through them.
            foreach (Engine.Trade q in questsUsingThisItem)
            {
                if (q.RequiredItems[sKey] <= dQty)
                {//Check the other items.
                    bool bHasAllItems = true;
                    foreach (KeyValuePair<string, decimal> kvpRequirement in q.RequiredItems)
                    {
                        if (string.Equals(kvpRequirement.Key, sKey))
                            continue;
                        if (PlayerInventory[kvpRequirement.Key] < kvpRequirement.Value)
                        {
                            bHasAllItems = false;
                            break;
                        }
                    }
                    if (bHasAllItems)
                    {
                        FireOnQuestReadyEvent(q);
                    }
                }
            }

        }
        #endregion

        #region Quest Ready Event
        public delegate void TradeReady(Trade ThisTradeID);
        public event TradeReady OnTradeReady;
        private void FireOnQuestReadyEvent(Trade ThisQuestID)
        {
            if (OnTradeReady != null)
            {
                Delegate[] invokeDelegateArray = OnTradeReady.GetInvocationList();
                for (int i = 0; i < invokeDelegateArray.Length; i++)
                    invokeDelegateArray[i].DynamicInvoke(ThisQuestID);
            }
        }
        private void _TradeAvailable_EventHandler_Internal(Trade QuestID)
        {
            CallToButton(QuestID);
        }
        #endregion 

        private void CallToButton(Trade theTrade)
        {
            Program.myQuestMenu.EnableTheTradeButton(theTrade);
        }

        #region Quests

        public Trade FindTradeID(string ClickedID)
        {
            foreach (KeyValuePair<string, Trade> kvp in AllTrades)
            {
                if (kvp.Key == ClickedID)
                    return kvp.Value;
            }
            return null;
        }

        public bool CanCompleteTrade(Engine.Trade qThisTrade)
        {
            bool myRes = true;
            foreach (KeyValuePair<string, decimal> kvp in qThisTrade.RequiredItems)
            {//for every required item (kvp, which is the item key and required quantity)
                if (GetQty(kvp.Key) < kvp.Value)
                    return false;
            }
            return myRes;//we only reach this point if we have enough of all items, so we can complete the quest
        }

        public void CompleteTheTrade(Trade myQuest)
        {
            Program.Status.ApplyTradeRewards(myQuest);
            Program.Status.ApplyTradeRequirements(myQuest);
        }

        public void ApplyTradeRequirements(Engine.Trade qThisTrade)
        {
            foreach (KeyValuePair<string, decimal> kvp in qThisTrade.RequiredItems)
            {
                Item _Reduce = AllItemsInTheGame[kvp.Key];

                if (_Reduce != null)
                {
                    decimal dcurQty = GetQty(kvp.Key);
                    if (!PlayerInventory.ContainsKey(kvp.Key))
                        PlayerInventory.Add(kvp.Key, 0);

                    if (!_Reduce.MaxInvQuantity.HasValue || (dcurQty - kvp.Value) <= _Reduce.MaxInvQuantity.Value)
                    {
                        {
                            UpdateInventory(kvp.Key, (kvp.Value * -1));
                        }
                    }
                }
            }
        }

        public void ApplyTradeRewards(Engine.Trade qThisTrade)
        {
            foreach (KeyValuePair<string, decimal> kvp in qThisTrade.RewardItems)
            {
                Item _Add = AllItemsInTheGame[kvp.Key];

                if (_Add != null)
                {
                    decimal dcurQty = GetQty(kvp.Key);
                    if (!PlayerInventory.ContainsKey(kvp.Key))
                        PlayerInventory.Add(kvp.Key, 0);

                    if (!_Add.MaxInvQuantity.HasValue || (dcurQty + kvp.Value) <= _Add.MaxInvQuantity.Value)
                    {
                        {
                            UpdateInventory(kvp.Key, kvp.Value);
                        }
                    }
                }
            }
        }

        #endregion

        #region Buying Items
        /// <summary>
        /// Gets the quantity of an item
        /// </summary>
        /// <param name="sInventoryKey"></param>
        /// <returns></returns>
        public decimal GetQty(string sInventoryKey)
        {
            if (!PlayerInventory.ContainsKey(sInventoryKey))
            {
                PlayerInventory.Add(sInventoryKey, 0m);
            }
            return PlayerInventory[sInventoryKey];
        }
        /// <summary>
        /// Attempts to update the inventory as well as labels
        /// </summary>
        /// <param name="sInventoryKey"></param>
        /// <param name="dQuantityDelta"></param>
        public void UpdateInventory(string sInventoryKey, decimal dQuantityDelta)
        {
            if (!PlayerInventory.ContainsKey(sInventoryKey))
            {
                PlayerInventory.Add(sInventoryKey, 0m);
            }
            decimal dInventoryQty = (PlayerInventory[sInventoryKey] + dQuantityDelta);

            PlayerInventory[sInventoryKey] = dInventoryQty;
            FireInventoryUpdatedEvent(sInventoryKey, dInventoryQty);
        }
        /// <summary>
        /// Checks if the user has the required machine
        /// </summary>
        /// <param name="_Add"></param>
        /// <returns></returns>
        private bool ItemHasRequiredMachines(Item _Add)
        {
            bool bHaveAllMachines = true;
            if (_Add.RequiredMachine != null)
            {
                foreach (KeyValuePair<string, decimal> kvpMachine in _Add.RequiredMachine)
                {
                    if (GetQty(kvpMachine.Key) < kvpMachine.Value)
                    {
                        bHaveAllMachines = false;
                        break;
                    }
                }
            }
            return bHaveAllMachines;
        }
        /// <summary>
        /// Boolean to show if the user has all of the requirements (machine + currency)
        /// </summary>
        /// <param name="addItem"></param>
        /// <param name="AddQty"></param>
        /// <returns></returns>
        public bool AbleToBuyTheItem(string addItem, decimal AddQty)
        {
            Item _Add = AllItemsInTheGame[addItem];
            if (ItemHasRequiredMachines(_Add) && HavePurchaseRequirements(addItem, AddQty))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Checks if the user has all fo the purchase requirements
        /// </summary>
        /// <param name="sItemID"></param>
        /// <param name="dPurchaseQty"></param>
        /// <returns></returns>
        public bool HavePurchaseRequirements(string sItemID, decimal dPurchaseQty)
        {
            Item myItem = AllItemsInTheGame[sItemID]; 
            bool CanIGetIt = true;
            {
                foreach (KeyValuePair<string, decimal> kvp in myItem.PurchaseRequirements)
                {
                    decimal? ReqQty = CheckItemPrice(sItemID, dPurchaseQty);
                    decimal myQty = GetQty(kvp.Key);

                    if (myQty >= ReqQty)
                    {
                        CanIGetIt = true; ;
                    }
                    else
                    {
                        CanIGetIt = false;
                        break;
                    }
                }
                if (myItem.RequiredSpecialItem != null)
                {
                    foreach (KeyValuePair<string, decimal> spItem in myItem.RequiredSpecialItem)
                    {
                        decimal myReqQty = GetQty(spItem.Key);
                        if (myReqQty >= spItem.Value)
                            CanIGetIt = true;
                        else
                            CanIGetIt = false;
                        break;
                    }
                }
            }
            return CanIGetIt;
        }
        /// <summary>
        /// Checks the price of the item depending on all of the requirements 
        /// </summary>
        /// <param name="sItemID"></param>
        /// <param name="dQty"></param>
        /// <returns></returns>
        public decimal? CheckItemPrice(string sItemID, decimal dQty = 1)
        {
            Item item = AllItemsInTheGame[sItemID];

            for (int i = 0; i < item.PurchaseRequirements.Count; i++)
            {
                if (item.PriceType == "Dynamic")
                {
                    decimal? BasePriceA = GetDynamicPrice(sItemID);
                    return BasePriceA;
                }
                else if (item.PriceType == "Static")
                {
                    decimal? BasePriceB = GetStaticPrice(sItemID);
                    return BasePriceB;
                }
            }
            return null;
        }
        /// <summary>
        /// Adds the item to inventory; goes to the UpdateAfterPurchase to add/remove items
        /// </summary>
        /// <param name="iAdd"></param>
        /// <param name="AddQty"></param>
        /// <param name="InventoryAdjustedCallback"></param>
        /// <param name="InventoryAdjustmentFailedCallback"></param>
        public void AddItemToInventory(string iAdd, decimal AddQty = 1m, Action<string> InventoryAdjustedCallback = null, Action<string> InventoryAdjustmentFailedCallback = null)
        {
            Item _Add = AllItemsInTheGame[iAdd];
            if (_Add != null)
            {
                decimal dCurQty = GetQty(iAdd);

                if (_Add.PurchaseRequirements == null)
                    UpdateInventory(iAdd, AddQty);
                else if (AbleToBuyTheItem(iAdd, AddQty))
                {
                    UpdateAfterPurchase(iAdd, AddQty);
                    if (InventoryAdjustedCallback != null)
                        InventoryAdjustedCallback(iAdd);
                }
                else
                {
                    if (InventoryAdjustmentFailedCallback != null)
                        InventoryAdjustmentFailedCallback(iAdd);
                }
            }
        }
        /// <summary>
        /// Updates affected items after HavePurchaseRequirements validates requirements
        /// </summary>
        /// <param name="addItem"></param>
        /// <param name="AddAmt"></param>
        public void UpdateAfterPurchase(string addItem, decimal AddAmt)
        {
            Item _Add = AllItemsInTheGame[addItem];
            foreach (KeyValuePair<string, decimal> kvp in _Add.PurchaseRequirements)
            {
                decimal redQty = Convert.ToDecimal(CheckItemPrice(addItem, AddAmt));
                UpdateInventory(kvp.Key, redQty * -1);
            }
            UpdateInventory(addItem, AddAmt);
        }
        /// <summary>
        /// Method specifically for Autclickers, as they are the only dynamic items in the game.
        /// </summary>
        /// <param name="sItemID"></param>
        /// <returns></returns>
        public decimal? GetDynamicPrice(string sItemID, decimal dItemQty = 1)
        {

            Item items = AllItemsInTheGame[sItemID];
            decimal dOutput = 0m;
            decimal dQty = GetQty(sItemID) + dItemQty;



            if (items.PurchaseRequirements != null)
            {
                decimal curQty = dQty;
                for (int i = 0; i < dItemQty; i++)
                {

                    foreach (KeyValuePair<string, decimal> kvp in items.PurchaseRequirements)
                    {
                        decimal dBasePrice = kvp.Value;
                        decimal dPriceAdjustment = Convert.ToDecimal(Math.Pow(1.419d, Convert.ToDouble(dQty)));
                        decimal dNextPrice = dPriceAdjustment * dBasePrice;

                        if (dNextPrice >= dBasePrice)
                        {
                            dOutput = Math.Round(dNextPrice);
                            return dOutput;
                        }
                        else
                            return dBasePrice;
                    }
                    curQty = dQty + 1;
                }
            }
            return null;
        }
        /// <summary>
        /// Gets the price of all the items other than autoclickers
        /// </summary>
        /// <param name="sItemID"></param>
        /// <returns></returns>
        public decimal? GetStaticPrice(string sItemID)
        {

            Item items = AllItemsInTheGame[sItemID];
            if (items.PurchaseRequirements != null)
            {
                foreach (KeyValuePair<string, decimal> kvp in items.PurchaseRequirements)
                {
                    decimal staticPrice = kvp.Value;

                    return staticPrice;
                }
            }
            return null;
        }
        #endregion

        #region Specific Class-Related Methods
        /// <summary>
        /// Gets the class of an item
        /// </summary>
        /// <param name="sClass"></param>
        /// <returns></returns>
        public string GetClass(string sClass)
        {
            {
                foreach (KeyValuePair<string, Item> kvp in AllItemsInTheGame)
                {
                    {
                        if (kvp.Value.Class.Contains(sClass) && GetQty(kvp.Key) <= 0)
                        {
                            return kvp.Key;
                        }
                    }
                }
                return null;
            }
        }
        /// <summary>
        /// Gets all the items in a specific class
        /// </summary>
        /// <param name="sClass"></param>
        /// <returns></returns>
        public List<string> GetItemsInClass(string sClass)
        {
            List<string> result = new List<string>();
            foreach (KeyValuePair<string, Item> kvp in AllItemsInTheGame)
            {
                if (kvp.Value.Class.Contains(sClass))
                {
                    result.Add(kvp.Key);
                }
            }
            return result;
        }
        /// <summary>
        /// Gets what's in the player's inventory based on class
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="minQty"></param>
        /// <returns></returns>
        public List<string> GetItemsInClassFromInventory(string sClass, decimal minQty = 1)
        {
            List<string> result = new List<string>();
            foreach (KeyValuePair<string, Item> kvp in AllItemsInTheGame)
            {
                if (kvp.Value.Class.Contains(sClass) && PlayerInventory[kvp.Key] >= minQty)
                {
                    result.Add(kvp.Key);
                }
            }
            return result;
        }
        /// <summary>
        /// Gets the item/s generated from factories 
        /// </summary>
        /// <param name="ClassName"></param>
        /// <returns></returns>
        public string GetGeneratedItem(string ClassName)
        {
            foreach (KeyValuePair<string, Item> kvp in AllItemsInTheGame)
            {
                {
                    if (kvp.Value.Class.Contains(ClassName) && GetQty(kvp.Key) >= 1)
                    {
                        return kvp.Value.ItemGenerated;
                    }
                }
            }
            return null;
        }
        #endregion
    }
}