﻿using Engine;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class BasicClicker : Form
    {
        Collectibles collMenu = new Collectibles();
        UserInventory userInv = new UserInventory();
        MarketMenu myQuest = new MarketMenu();


        public BasicClicker()
        {
            InitializeComponent();
        }

        private void BasicClicker_Load(object sender, EventArgs e)
        {
            collMenu.Hide();
            userInv.Hide();
            myQuest.Hide();
            Updater.Start();
            AddItems.Start();
        }

        #region Clicker Buttons
        private void btnCChipsPH_Click(object sender, EventArgs e)
        {
            if (e is System.Windows.Forms.MouseEventArgs)
            {
                Program.Status.UpdateInventory("ChocoChips", 1);
            }
        }
        private void btnCandyPH_Click(object sender, EventArgs e)
        {
            if (e is System.Windows.Forms.MouseEventArgs)
            {
                Program.Status.UpdateInventory("Candy", 1);
            }
        }
        private void btnSugarCubePH_Click_1(object sender, EventArgs e)
        {
            if (e is System.Windows.Forms.MouseEventArgs)
            {
                Program.Status.UpdateInventory("SugarCube", 1);
            }
        }

        #endregion



        private void VillainTimer_Tick(object sender, EventArgs e)
        {
            var chill = new Collectibles();
            var Vill = new Villain_();
            Random r = new Random();
            while (true)
            {
                chill.Close();
                Vill.ShowDialog();
            }
        }
        private void InventoryButton_Click(object sender, EventArgs e)
        {
            if (userInv.Visible)
                userInv.Hide();
            else
                userInv.Show();
        }

        private void MarketButton_Click(object sender, EventArgs e)
        {
            if (myQuest.Visible)
                myQuest.Hide();
            else
                myQuest.Show();
        }

        private void CollectiblesButton_Click(object sender, EventArgs e)
        {
            if (collMenu.Visible)
                collMenu.Hide();
            else
                collMenu.Show();
        }

        // TODO: Get rid of this when I'm done fixing things
        private void GimmeEverything_Click(object sender, EventArgs e)
        {
            Program.Status.UpdateInventory("MintFactory", 1);
            Program.Status.UpdateInventory("Candy", 100000000000000);
            Program.Status.UpdateInventory("ChocoChips", 100000000000000);
            Program.Status.UpdateInventory("SugarCube", 100000000000000);
            Program.Status.UpdateInventory("PopCandy", 1000000);
            Program.Status.UpdateInventory("CandyCorn", 1000000);
            Program.Status.UpdateInventory("Gum", 1000000);
            Program.Status.UpdateInventory("CandyRing", 1000000);
            Program.Status.UpdateInventory("Lolli", 1000000);
            Program.Status.UpdateInventory("Jawbreaker", 1000000);
            Program.Status.UpdateInventory("Mint", 1000000);
            Program.Status.UpdateInventory("Gummy", 1000000);
            Program.Status.UpdateInventory("Taffy", 1000000);
            Program.Status.UpdateInventory("Brittle", 1000000);
            Program.Status.UpdateInventory("Truffle", 1000000);
            Program.Status.UpdateInventory("Toffee", 1000000);
            Program.Status.UpdateInventory("Fudge", 1000000);
            Program.Status.UpdateInventory("Buttercream", 1000000);
            Program.Status.UpdateInventory("Patties", 1000000);
        }

        private void LabelUpdate()
        {
            Dictionary<string, string> uh = new Dictionary<string, string>();
            uh.Add("Candy", "AutoClicker");
            uh.Add("ChocoChips", "ChocoClicker");
            uh.Add("SugarCube", "SugarCubeClicker");

            foreach (KeyValuePair<string, string> kvp in uh)
            {
                lblCandyDust.Text = Program.Status.PlayerInventory["Candy"].ToString();
                lblChocoChips.Text = Program.Status.PlayerInventory["ChocoChips"].ToString();
                lblSugarCube.Text = Program.Status.PlayerInventory["SugarCube"].ToString();
            }

        }
        private void RequirementSearch()
        {
            List<string> machList = Program.Status.GetItemsInClassFromInventory("Machine");

            if (machList.Count >= 15)
            {
                btnSugarCubePH.Visible = true;
                btnSugarCubePH.Enabled = true;

                btnCChipPH.Visible = true;
                btnCChipPH.Enabled = true;
            }
            else if (machList.Count >= 9)
            {
                btnCChipPH.Visible = true;
                btnCChipPH.Enabled = true;
            }
        }

        /// <summary>
        /// Updates labels as well as checks the user's inventory if they have the required amount for a certain item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Updater_Tick(object sender, EventArgs e)
        {
            LabelUpdate();
            RequirementSearch();
        }

        private void AutoAddItems(object sender, EventArgs e)
        {
            AutoclickerAdd();
        }

        public void AutoclickerAdd()
        {
            Dictionary<string, string> uh = new Dictionary<string, string>();
            uh.Add("Candy", "CandyClicker");
            uh.Add("ChocoChips", "ChocoClicker");
            uh.Add("SugarCube", "SugarCubeClicker");

            foreach (KeyValuePair<string, string> kvp in uh)
            {
                decimal myQty = Program.Status.GetQty(kvp.Key);
                decimal clickerQty = Program.Status.GetQty(kvp.Value);

                Program.Status.PlayerInventory[kvp.Key] += clickerQty;
                lblCandyDust.Text = Program.Status.PlayerInventory["Candy"].ToString();
                lblChocoChips.Text = Program.Status.PlayerInventory["ChocoChips"].ToString();
                lblSugarCube.Text = Program.Status.PlayerInventory["SugarCube"].ToString();
            }
        }

        /// <summary>
        /// Creates a tooltip balloon over the Trade Market Button to notify the user that a quest is available to turn in
        /// </summary>
        /// <param name="QuestName">the Quest that is available to be turned in</param>
        public void HeyIHaveATradeReady(Trade QuestName) // Okay so this doesn't work either. This functions is called from the timer event I created in the QuestMenu in an attempt to make the tooltip appear when the buttons are enabled. Before, NotifyMe was a dynamically created tooktip, but now I added a tooltip to the form itself to make things easier. 
        {

            //NotifyMe.Show(String.Format("{0} is available to trade with you!", QuestName.Name), buttonMarket); // the string that will appear, and the button that it will sit on (Trade Market)
            // What I want to happen:
            // When TradeReady event fires, it will fire this function as well as the button enabler function.
            // NotifyMe is a balloon tooltip that'll appear regardless of if the form is active. This tooltip has already been added to the form. 
            // It simply says something like "Hey player, you have the necessary items for this trade!"
            // Then disappears and reappears when the TradeReady event is fired again.
        }
    }
}