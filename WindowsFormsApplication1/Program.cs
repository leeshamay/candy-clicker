﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using Engine;

namespace WindowsFormsApplication1
{
    static class Program
    {
        public static GameStatus Status;
        public static BasicClicker bClicker;
        public static Collectibles collectibles;
        public static Villain_ villain;
        public static MarketMenu myQuestMenu;
        public static UserInventory userInv;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (!string.IsNullOrWhiteSpace(args[0]))
                {
                    System.Diagnostics.Trace.WriteLine(string.Format("Reading game content from '{0}'", args[0]));//Write something out to the output window.
                    string sContents = File.ReadAllText(args[0]);
                    JObject jNew = JObject.Parse(sContents);
                    Status = new GameStatus(jNew);
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    bClicker = new BasicClicker();
                    collectibles = new Collectibles();
                    villain = new Villain_();
                    userInv = new UserInventory();
                    myQuestMenu = new MarketMenu();
                    Application.Run(new BasicClicker());
                }
            }
        }
    }
}
