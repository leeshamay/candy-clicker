﻿using Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class UserInventory : Form
    {

        List<Label> LabelsToUpdate = new List<Label>();
        List<Button> ButtonsToEnable = new List<Button>();
        List<Label> LblName = new List<Label>();
        private void UserInventory_Load(object sender, EventArgs e)
        {
            // ???
        }
        public Dictionary<string, Label> myInventoryDisplay = new Dictionary<string, Label>();
        public UserInventory()
        {
            InitializeComponent();
            
            Stuff();
            ReqCheck.Start();
            LabelUpdate.Start();
            Program.Status.OnItemQtyChanged += Status_OnInventoryUpdated;
            FirstPriceLabel();
        }
        public void Stuff()
        {
            LoadAllTheButtons();
            mfLabels();
            specialLabels();
            SpecialQtyLabels();
            itemLabels();
            itemQtyLabels();
        }

        private void NotEnoughMoneyDesu(string sItemCode)
        {
            MessageBox.Show(string.Format("You require more minerals to buy that!"));
        }

        void Status_OnInventoryUpdated(string sKey, decimal dQty)
        {
            if (myInventoryDisplay.ContainsKey(sKey))
                myInventoryDisplay[sKey].Text = dQty.ToString();
        }

        public void LoadAllTheButtons()
        {
            {
                int iCurrentX = 125;
                int iCurrentY = 8;
                int iRowSpacing = 30;

                string[] buttonName = { "Autoclicker", "Item" };
                for (int j = 0; j < buttonName.Length; j++)
                {
                    Button newButton = new Button();
                    newButton.Name = string.Format("button{0}", buttonName[j]);
                    List<string> bmf = Program.Status.GetItemsInClass(buttonName[j]);


                    for (int i = 0; i < bmf.Count; i++)
                    {
                        Item iThisItem = Program.Status.AllItemsInTheGame[bmf[i]];
                        Button bbmf = new Button();
                        bbmf.Tag = iThisItem.ID;
                        bbmf.Name = "btn" + bbmf.Tag;
                        bbmf.Text = "Buy";
                        bbmf.BackColor = Color.Transparent;
                        bbmf.Visible = true;
                        bbmf.Click += new EventHandler(Item_Click);
                        bbmf.Font = new Font("Georgia", 6);
                        iCurrentY = iCurrentY + iRowSpacing;
                        bbmf.Location = new Point(iCurrentX, iCurrentY);
                        bbmf.Size = new System.Drawing.Size(40, 15);
                        Items.Controls.Add(bbmf);
                        bbmf.Enabled = false;
                        Items.AutoScroll = true;
                        ButtonsToEnable.Add(bbmf);
                    }
                }
            }

            {
                int iCurrentX = 125;
                int iCurrentY = 8;
                int iRowSpacing = 30;

                string[] buttonName = { "Machine", "Factory" };
                for (int j = 0; j < buttonName.Length; j++)
                {
                    Button newButton = new Button();
                    newButton.Name = string.Format("button{0}", buttonName[j]);
                    List<string> bmf = Program.Status.GetItemsInClass(buttonName[j]);


                    for (int i = 0; i < bmf.Count; i++)
                    {
                        Item iThisItem = Program.Status.AllItemsInTheGame[bmf[i]];
                        Button bbmf = new Button();
                        bbmf.Tag = iThisItem.ID;
                        bbmf.Name = "btn" + bbmf.Tag;
                        bbmf.Text = "Buy";
                        bbmf.BackColor = Color.Transparent;
                        bbmf.Visible = true;
                        bbmf.Click += new EventHandler(machFact_Click);
                        bbmf.Font = new Font("Georgia", 6);
                        iCurrentY = iCurrentY + iRowSpacing;
                        bbmf.Location = new Point(iCurrentX, iCurrentY);
                        bbmf.Size = new System.Drawing.Size(40, 15);
                        MachFact.Controls.Add(bbmf);
                        bbmf.Enabled = false;
                        MachFact.AutoScroll = true;
                        ButtonsToEnable.Add(bbmf);
                    }
                }
            }
        }

        public void SpecialQtyLabels()
        {
            {
                int iCurrentX = 200;
                int iCurrentY = 10;
                int iRowSpacing = 30;
                string[] labelname = { "Special" };
                for (int j = 0; j < labelname.Length; j++)
                {
                    List<string> lmf = Program.Status.GetItemsInClass(labelname[j]);

                    for (int i = 0; i < lmf.Count; i++)
                    {
                        Item iThisItem = Program.Status.AllItemsInTheGame[lmf[i]];
                        Label llmf = new Label();
                        llmf.Tag = iThisItem.ID;
                        llmf.Name = string.Format("lbl{0}btn", lmf[i]);
                        llmf.Text = Program.Status.GetQty(iThisItem.ID).ToString();
                        llmf.BackColor = Color.Transparent;
                        llmf.Visible = true;
                        iCurrentY = iCurrentY + iRowSpacing;
                        llmf.Location = new Point(iCurrentX, iCurrentY);
                        llmf.Size = new System.Drawing.Size(100, 20);
                        SpecialItems.Controls.Add(llmf);
                        SpecialItems.AutoScroll = true;

                        LabelsToUpdate.Add(llmf);
                    }
                }
            }

        }

        public void itemLabels()
        {
            int iCurrentX = 10;
            int iCurrentY = 10;
            int iRowSpacing = 30;
            string[] labelname = { "Autoclicker", "Item" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = Program.Status.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Item iThisItem = Program.Status.AllItemsInTheGame[lmf[i]];

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}", lmf[i]);
                    llmf.Text = iThisItem.Name;
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(100, 20);
                    Items.Controls.Add(llmf);
                    Items.AutoScroll = true;

                    LblName.Add(llmf);
                }
            }
        }

        public void itemQtyLabels()
        {
            int iCurrentX = 200;
            int iCurrentY = 10;
            int iRowSpacing = 30;
            string[] labelname = { "Autoclicker", "Item" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = Program.Status.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Item iThisItem = Program.Status.AllItemsInTheGame[lmf[i]];
                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}", lmf[i]);
                    llmf.Text = Program.Status.GetQty(iThisItem.ID).ToString();
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(100, 20);
                    Items.Controls.Add(llmf);
                    Items.AutoScroll = true;

                    LabelsToUpdate.Add(llmf);


                }
            }
        }

        private void Item_Click(object sender, EventArgs e)
        {
            Button addItem = sender as Button;
            List<Label> match = LblName;
            Program.Status.AddItemToInventory(addItem.Tag.ToString());
            foreach (Label myMatch in match)
            {
                if (myMatch.Tag == addItem.Tag)
                {
                    Text_ValueChanged(myMatch, e);
                }
            }

        }

        public void mfLabels()
        {
            int iCurrentX = 10;
            int iCurrentY = 10;
            int iRowSpacing = 30;
            string[] labelname = { "Machine", "Factory" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = Program.Status.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Item iThisItem = Program.Status.AllItemsInTheGame[lmf[i]];
                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}", lmf[i]);
                    llmf.Text = iThisItem.Name;
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(100, 20);
                    MachFact.Controls.Add(llmf);
                    MachFact.AutoScroll = true;

                    LblName.Add(llmf);
                }
            }
        }

        private void machFact_Click(object sender, EventArgs e)
        {
            Label myLabel = new Label();
            Button addItem = sender as Button;
            Program.Status.AddItemToInventory(addItem.Tag.ToString());
            if (Program.Status.GetQty(addItem.Tag.ToString()) >= 1)
            {
                addItem.Visible = false;
                myLabel.Name = String.Format("lbl{0}//////", addItem.Name);
                myLabel.Visible = true;
            }
        }

        public void specialLabels()
        {
            int iCurrentX = 10;
            int iCurrentY = 10;
            int iRowSpacing = 30;
            string[] labelname = { "Special" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = Program.Status.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Item iThisItem = Program.Status.AllItemsInTheGame[lmf[i]];
                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}btn", lmf[i]);
                    llmf.Text = iThisItem.Name;
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(100, 20);
                    SpecialItems.Controls.Add(llmf);
                    SpecialItems.AutoScroll = true;
                }
            }
        }

        private void FactoryTimers_Tick(object sender, EventArgs e)
        {
            List<string> factoryList = Program.Status.GetItemsInClassFromInventory("Factory");
            foreach (string sFactoryID in factoryList)
            {
                if (!string.IsNullOrWhiteSpace(sFactoryID) && Program.Status.AllItemsInTheGame.ContainsKey(sFactoryID))
                {
                    Item factoryItem = Program.Status.AllItemsInTheGame[sFactoryID];
                    if (!string.IsNullOrWhiteSpace(factoryItem.ItemGenerated))
                    {
                        Program.Status.AddItemToInventory(factoryItem.ItemGenerated);
                    }
                }
            }
        }
        /// <summary>
        /// Looks at the Label text, gets the new qty the item it's labelled for, and replaces the previous text with the new qty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelUpdate_Tick(object sender, EventArgs e)
        {
            List<Label> myLabels = LabelsToUpdate; // Gets the list of labels to update
            foreach (Label curLabel in myLabels)
            {
                for (int i = 0; i < LabelsToUpdate.Count; i++)
                {
                    string getQty = (string)curLabel.Tag; // Gets the tag of the label to identify the item
                    decimal myQty = Program.Status.GetQty(getQty);  // Gets the qty 
                    curLabel.Text = Convert.ToString(myQty);  // Converts the qty to string and displays the new amount
                }
            }
        }

        private void ReqCheck_Timer(object sender, EventArgs e)
        {
            List<Button> myButtons = ButtonsToEnable;
            foreach (Button bButton in myButtons)
            {
                if (Program.Status.AbleToBuyTheItem((string)bButton.Tag, 1))
                {
                    bButton.Enabled = true;
                }
                else
                    bButton.Enabled = false;
            }

        }
        private void FirstPriceLabel()
        {
            List<Label> myLbl = LblName;
            foreach (Label Lbl in myLbl)
            {
                Item itemID = Program.Status.AllItemsInTheGame[(string)Lbl.Tag];
                foreach (KeyValuePair<string, decimal> kvp in itemID.PurchaseRequirements)
                {
                    if (itemID.RequiredSpecialItem.Count > 0)
                    {
                        foreach (KeyValuePair<string, decimal> spItem in itemID.RequiredSpecialItem)
                        {
                            ThisToolTip.SetToolTip(Lbl, String.Format("{0} : {1} / {2} : {3}", kvp.Key, kvp.Value, spItem.Key, spItem.Value));
                        }
                    }
                    else
                    {
                        decimal? nQty = Program.Status.CheckItemPrice((string)Lbl.Tag);
                        ThisToolTip.SetToolTip(Lbl, String.Format("{0} : {1}", kvp.Key, nQty));
                    }
                }
            }
        }


        public void Text_ValueChanged(object sender, EventArgs e)
        {
            Label myLbl = sender as Label;
            Item itemID = Program.Status.AllItemsInTheGame[(string)myLbl.Tag];
            foreach (KeyValuePair<string, decimal> kvp in itemID.PurchaseRequirements)
            {
                if (itemID.RequiredSpecialItem.Count > 0)
                {
                    foreach (KeyValuePair<string, decimal> spItem in itemID.RequiredSpecialItem)
                    {
                        ThisToolTip.SetToolTip(myLbl, String.Format("{0} : {1} / {2} : {3}", kvp.Key, kvp.Value, spItem.Key, spItem.Value));
                    }
                }
                else
                {
                    decimal? nQty = Program.Status.CheckItemPrice((string)myLbl.Tag);
                    ThisToolTip.SetToolTip(myLbl, String.Format("{0} : {1}", kvp.Key, nQty));
                }
            }
        }

    }
}