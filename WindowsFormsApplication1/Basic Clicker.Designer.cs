﻿namespace WindowsFormsApplication1
{
    partial class BasicClicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BasicClicker));
            this.btnCChipPH = new System.Windows.Forms.Button();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem35 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem36 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem37 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem38 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem46 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem47 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem48 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem49 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem50 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem51 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem52 = new System.Windows.Forms.ToolStripMenuItem();
            this.brittleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem53 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem54 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem55 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem56 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem57 = new System.Windows.Forms.ToolStripMenuItem();
            this.truffleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem58 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem59 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem60 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem61 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem62 = new System.Windows.Forms.ToolStripMenuItem();
            this.toffeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem63 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem64 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem65 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem66 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem67 = new System.Windows.Forms.ToolStripMenuItem();
            this.fudgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem68 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem69 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem70 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem71 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem72 = new System.Windows.Forms.ToolStripMenuItem();
            this.buttercreamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem78 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem79 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem80 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem81 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem82 = new System.Windows.Forms.ToolStripMenuItem();
            this.pattiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem73 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem74 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem75 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem76 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem77 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCandyPH = new System.Windows.Forms.Button();
            this.buttonCollectibles = new System.Windows.Forms.Button();
            this.lblSugarCube = new System.Windows.Forms.Label();
            this.labelSugarCube = new System.Windows.Forms.Label();
            this.lblChocoChips = new System.Windows.Forms.Label();
            this.lblCandyDust = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSugarCubePH = new System.Windows.Forms.Button();
            this.VillainTimer = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.buttonMarket = new System.Windows.Forms.Button();
            this.InventoryButton = new System.Windows.Forms.Button();
            this.Updater = new System.Windows.Forms.Timer(this.components);
            this.AddItems = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnCChipPH
            // 
            this.btnCChipPH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCChipPH.BackgroundImage")));
            this.btnCChipPH.Enabled = false;
            this.btnCChipPH.Location = new System.Drawing.Point(243, 18);
            this.btnCChipPH.Name = "btnCChipPH";
            this.btnCChipPH.Size = new System.Drawing.Size(225, 101);
            this.btnCChipPH.TabIndex = 0;
            this.btnCChipPH.TabStop = false;
            this.btnCChipPH.UseVisualStyleBackColor = true;
            this.btnCChipPH.Visible = false;
            this.btnCChipPH.Click += new System.EventHandler(this.btnCChipsPH_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            this.toolStripMenuItem31.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            this.toolStripMenuItem32.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem33
            // 
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            this.toolStripMenuItem33.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem34
            // 
            this.toolStripMenuItem34.Name = "toolStripMenuItem34";
            this.toolStripMenuItem34.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem35
            // 
            this.toolStripMenuItem35.Name = "toolStripMenuItem35";
            this.toolStripMenuItem35.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem36
            // 
            this.toolStripMenuItem36.Name = "toolStripMenuItem36";
            this.toolStripMenuItem36.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem37
            // 
            this.toolStripMenuItem37.Name = "toolStripMenuItem37";
            this.toolStripMenuItem37.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem38
            // 
            this.toolStripMenuItem38.Name = "toolStripMenuItem38";
            this.toolStripMenuItem38.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem39
            // 
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            this.toolStripMenuItem39.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem40
            // 
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            this.toolStripMenuItem40.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem41
            // 
            this.toolStripMenuItem41.Name = "toolStripMenuItem41";
            this.toolStripMenuItem41.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem42
            // 
            this.toolStripMenuItem42.Name = "toolStripMenuItem42";
            this.toolStripMenuItem42.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem43
            // 
            this.toolStripMenuItem43.Name = "toolStripMenuItem43";
            this.toolStripMenuItem43.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem44
            // 
            this.toolStripMenuItem44.Name = "toolStripMenuItem44";
            this.toolStripMenuItem44.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem45
            // 
            this.toolStripMenuItem45.Name = "toolStripMenuItem45";
            this.toolStripMenuItem45.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem46
            // 
            this.toolStripMenuItem46.Name = "toolStripMenuItem46";
            this.toolStripMenuItem46.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem47
            // 
            this.toolStripMenuItem47.Name = "toolStripMenuItem47";
            this.toolStripMenuItem47.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem48
            // 
            this.toolStripMenuItem48.Name = "toolStripMenuItem48";
            this.toolStripMenuItem48.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem49
            // 
            this.toolStripMenuItem49.Name = "toolStripMenuItem49";
            this.toolStripMenuItem49.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem50
            // 
            this.toolStripMenuItem50.Name = "toolStripMenuItem50";
            this.toolStripMenuItem50.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem51
            // 
            this.toolStripMenuItem51.Name = "toolStripMenuItem51";
            this.toolStripMenuItem51.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem52
            // 
            this.toolStripMenuItem52.Name = "toolStripMenuItem52";
            this.toolStripMenuItem52.Size = new System.Drawing.Size(32, 19);
            // 
            // brittleToolStripMenuItem
            // 
            this.brittleToolStripMenuItem.Name = "brittleToolStripMenuItem";
            this.brittleToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem53
            // 
            this.toolStripMenuItem53.Name = "toolStripMenuItem53";
            this.toolStripMenuItem53.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem54
            // 
            this.toolStripMenuItem54.Name = "toolStripMenuItem54";
            this.toolStripMenuItem54.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem55
            // 
            this.toolStripMenuItem55.Name = "toolStripMenuItem55";
            this.toolStripMenuItem55.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem56
            // 
            this.toolStripMenuItem56.Name = "toolStripMenuItem56";
            this.toolStripMenuItem56.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem57
            // 
            this.toolStripMenuItem57.Name = "toolStripMenuItem57";
            this.toolStripMenuItem57.Size = new System.Drawing.Size(32, 19);
            // 
            // truffleToolStripMenuItem
            // 
            this.truffleToolStripMenuItem.Name = "truffleToolStripMenuItem";
            this.truffleToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem58
            // 
            this.toolStripMenuItem58.Name = "toolStripMenuItem58";
            this.toolStripMenuItem58.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem59
            // 
            this.toolStripMenuItem59.Name = "toolStripMenuItem59";
            this.toolStripMenuItem59.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem60
            // 
            this.toolStripMenuItem60.Name = "toolStripMenuItem60";
            this.toolStripMenuItem60.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem61
            // 
            this.toolStripMenuItem61.Name = "toolStripMenuItem61";
            this.toolStripMenuItem61.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem62
            // 
            this.toolStripMenuItem62.Name = "toolStripMenuItem62";
            this.toolStripMenuItem62.Size = new System.Drawing.Size(32, 19);
            // 
            // toffeeToolStripMenuItem
            // 
            this.toffeeToolStripMenuItem.Name = "toffeeToolStripMenuItem";
            this.toffeeToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem63
            // 
            this.toolStripMenuItem63.Name = "toolStripMenuItem63";
            this.toolStripMenuItem63.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem64
            // 
            this.toolStripMenuItem64.Name = "toolStripMenuItem64";
            this.toolStripMenuItem64.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem65
            // 
            this.toolStripMenuItem65.Name = "toolStripMenuItem65";
            this.toolStripMenuItem65.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem66
            // 
            this.toolStripMenuItem66.Name = "toolStripMenuItem66";
            this.toolStripMenuItem66.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem67
            // 
            this.toolStripMenuItem67.Name = "toolStripMenuItem67";
            this.toolStripMenuItem67.Size = new System.Drawing.Size(32, 19);
            // 
            // fudgeToolStripMenuItem
            // 
            this.fudgeToolStripMenuItem.Name = "fudgeToolStripMenuItem";
            this.fudgeToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem68
            // 
            this.toolStripMenuItem68.Name = "toolStripMenuItem68";
            this.toolStripMenuItem68.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem69
            // 
            this.toolStripMenuItem69.Name = "toolStripMenuItem69";
            this.toolStripMenuItem69.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem70
            // 
            this.toolStripMenuItem70.Name = "toolStripMenuItem70";
            this.toolStripMenuItem70.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem71
            // 
            this.toolStripMenuItem71.Name = "toolStripMenuItem71";
            this.toolStripMenuItem71.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem72
            // 
            this.toolStripMenuItem72.Name = "toolStripMenuItem72";
            this.toolStripMenuItem72.Size = new System.Drawing.Size(32, 19);
            // 
            // buttercreamToolStripMenuItem
            // 
            this.buttercreamToolStripMenuItem.Name = "buttercreamToolStripMenuItem";
            this.buttercreamToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem78
            // 
            this.toolStripMenuItem78.Name = "toolStripMenuItem78";
            this.toolStripMenuItem78.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem79
            // 
            this.toolStripMenuItem79.Name = "toolStripMenuItem79";
            this.toolStripMenuItem79.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem80
            // 
            this.toolStripMenuItem80.Name = "toolStripMenuItem80";
            this.toolStripMenuItem80.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem81
            // 
            this.toolStripMenuItem81.Name = "toolStripMenuItem81";
            this.toolStripMenuItem81.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem82
            // 
            this.toolStripMenuItem82.Name = "toolStripMenuItem82";
            this.toolStripMenuItem82.Size = new System.Drawing.Size(32, 19);
            // 
            // pattiesToolStripMenuItem
            // 
            this.pattiesToolStripMenuItem.Name = "pattiesToolStripMenuItem";
            this.pattiesToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem73
            // 
            this.toolStripMenuItem73.Name = "toolStripMenuItem73";
            this.toolStripMenuItem73.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem74
            // 
            this.toolStripMenuItem74.Name = "toolStripMenuItem74";
            this.toolStripMenuItem74.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem75
            // 
            this.toolStripMenuItem75.Name = "toolStripMenuItem75";
            this.toolStripMenuItem75.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem76
            // 
            this.toolStripMenuItem76.Name = "toolStripMenuItem76";
            this.toolStripMenuItem76.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem77
            // 
            this.toolStripMenuItem77.Name = "toolStripMenuItem77";
            this.toolStripMenuItem77.Size = new System.Drawing.Size(32, 19);
            // 
            // btnCandyPH
            // 
            this.btnCandyPH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCandyPH.BackgroundImage")));
            this.btnCandyPH.Location = new System.Drawing.Point(12, 18);
            this.btnCandyPH.Name = "btnCandyPH";
            this.btnCandyPH.Size = new System.Drawing.Size(225, 101);
            this.btnCandyPH.TabIndex = 5;
            this.btnCandyPH.TabStop = false;
            this.btnCandyPH.UseVisualStyleBackColor = true;
            this.btnCandyPH.Click += new System.EventHandler(this.btnCandyPH_Click);
            // 
            // buttonCollectibles
            // 
            this.buttonCollectibles.Location = new System.Drawing.Point(497, 223);
            this.buttonCollectibles.Name = "buttonCollectibles";
            this.buttonCollectibles.Size = new System.Drawing.Size(157, 55);
            this.buttonCollectibles.TabIndex = 119;
            this.buttonCollectibles.Text = "Collectibles";
            this.buttonCollectibles.UseVisualStyleBackColor = true;
            this.buttonCollectibles.Click += new System.EventHandler(this.CollectiblesButton_Click);
            // 
            // lblSugarCube
            // 
            this.lblSugarCube.AutoSize = true;
            this.lblSugarCube.Font = new System.Drawing.Font("Segoe Print", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSugarCube.Location = new System.Drawing.Point(162, 184);
            this.lblSugarCube.Name = "lblSugarCube";
            this.lblSugarCube.Size = new System.Drawing.Size(20, 24);
            this.lblSugarCube.TabIndex = 83;
            this.lblSugarCube.Text = "0";
            // 
            // labelSugarCube
            // 
            this.labelSugarCube.AutoSize = true;
            this.labelSugarCube.Font = new System.Drawing.Font("Segoe Print", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSugarCube.Location = new System.Drawing.Point(13, 184);
            this.labelSugarCube.Name = "labelSugarCube";
            this.labelSugarCube.Size = new System.Drawing.Size(95, 24);
            this.labelSugarCube.TabIndex = 81;
            this.labelSugarCube.Text = "Sugar Cube:";
            // 
            // lblChocoChips
            // 
            this.lblChocoChips.AutoSize = true;
            this.lblChocoChips.Font = new System.Drawing.Font("Segoe Print", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChocoChips.Location = new System.Drawing.Point(161, 160);
            this.lblChocoChips.Name = "lblChocoChips";
            this.lblChocoChips.Size = new System.Drawing.Size(20, 24);
            this.lblChocoChips.TabIndex = 59;
            this.lblChocoChips.Text = "0";
            // 
            // lblCandyDust
            // 
            this.lblCandyDust.AutoSize = true;
            this.lblCandyDust.Font = new System.Drawing.Font("Segoe Print", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCandyDust.Location = new System.Drawing.Point(161, 136);
            this.lblCandyDust.Name = "lblCandyDust";
            this.lblCandyDust.Size = new System.Drawing.Size(20, 24);
            this.lblCandyDust.TabIndex = 58;
            this.lblCandyDust.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 24);
            this.label2.TabIndex = 39;
            this.label2.Text = "Chocolate Chips:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 24);
            this.label1.TabIndex = 38;
            this.label1.Text = "Candy Dust: ";
            // 
            // btnSugarCubePH
            // 
            this.btnSugarCubePH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSugarCubePH.BackgroundImage")));
            this.btnSugarCubePH.Enabled = false;
            this.btnSugarCubePH.Location = new System.Drawing.Point(474, 18);
            this.btnSugarCubePH.Name = "btnSugarCubePH";
            this.btnSugarCubePH.Size = new System.Drawing.Size(225, 101);
            this.btnSugarCubePH.TabIndex = 9;
            this.btnSugarCubePH.TabStop = false;
            this.btnSugarCubePH.UseVisualStyleBackColor = true;
            this.btnSugarCubePH.Visible = false;
            this.btnSugarCubePH.Click += new System.EventHandler(this.btnSugarCubePH_Click_1);
            // 
            // VillainTimer
            // 
            this.VillainTimer.Interval = 1000000000;
            this.VillainTimer.Tick += new System.EventHandler(this.VillainTimer_Tick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(624, 137);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 87;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.GimmeEverything_Click);
            // 
            // buttonMarket
            // 
            this.buttonMarket.Location = new System.Drawing.Point(39, 223);
            this.buttonMarket.Name = "buttonMarket";
            this.buttonMarket.Size = new System.Drawing.Size(157, 55);
            this.buttonMarket.TabIndex = 126;
            this.buttonMarket.Text = "Trade Market";
            this.buttonMarket.UseVisualStyleBackColor = true;
            this.buttonMarket.Click += new System.EventHandler(this.MarketButton_Click);
            // 
            // InventoryButton
            // 
            this.InventoryButton.Location = new System.Drawing.Point(269, 223);
            this.InventoryButton.Name = "InventoryButton";
            this.InventoryButton.Size = new System.Drawing.Size(157, 55);
            this.InventoryButton.TabIndex = 127;
            this.InventoryButton.Text = "Inventory";
            this.InventoryButton.UseVisualStyleBackColor = true;
            this.InventoryButton.Click += new System.EventHandler(this.InventoryButton_Click);
            // 
            // Updater
            // 
            this.Updater.Enabled = true;
            this.Updater.Interval = 10;
            this.Updater.Tick += new System.EventHandler(this.Updater_Tick);
            // 
            // AddItems
            // 
            this.AddItems.Enabled = true;
            this.AddItems.Interval = 1000;
            this.AddItems.Tick += new System.EventHandler(this.AutoAddItems);
            // 
            // BasicClicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(718, 294);
            this.Controls.Add(this.InventoryButton);
            this.Controls.Add(this.buttonMarket);
            this.Controls.Add(this.buttonCollectibles);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnSugarCubePH);
            this.Controls.Add(this.btnCandyPH);
            this.Controls.Add(this.btnCChipPH);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSugarCube);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCandyDust);
            this.Controls.Add(this.labelSugarCube);
            this.Controls.Add(this.lblChocoChips);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "BasicClicker";
            this.Text = "Candy Clicker";
            this.Load += new System.EventHandler(this.BasicClicker_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCChipPH;
        private System.Windows.Forms.ToolStripMenuItem brittleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem truffleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toffeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fudgeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buttercreamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pattiesToolStripMenuItem;
        private System.Windows.Forms.Button btnCandyPH;
        private System.Windows.Forms.Label lblChocoChips;
        private System.Windows.Forms.Label lblCandyDust;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSugarCubePH;
        private System.Windows.Forms.Label lblSugarCube;
        private System.Windows.Forms.Label labelSugarCube;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem33;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem34;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem35;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem36;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem37;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem38;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem41;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem42;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem43;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem46;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem47;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem48;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem49;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem50;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem51;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem52;
        private System.Windows.Forms.Button buttonCollectibles;
        public System.Windows.Forms.Timer VillainTimer;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem53;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem54;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem55;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem56;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem57;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem58;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem59;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem60;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem61;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem62;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem63;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem64;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem65;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem66;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem67;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem68;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem69;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem70;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem71;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem72;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem78;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem79;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem80;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem81;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem82;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem73;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem74;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem75;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem76;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem77;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonMarket;
        private System.Windows.Forms.Button InventoryButton;
        private System.Windows.Forms.Timer Updater;
        private System.Windows.Forms.Timer AddItems;
    }
}