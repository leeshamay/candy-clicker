﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Engine
{
    public static class Extensions
    {
        public static bool Exists(this JToken jIsAToken, string sPropertyName)
        {
            if (jIsAToken != null)
            {
                JToken jProperty = jIsAToken.SelectToken(sPropertyName);
                if (jProperty != null)
                    return true;
            }
            return false;
        }
        public static string ParseValue(this JToken jIsAToken)
        {
            if (jIsAToken.Type == JTokenType.String)
                return (string)jIsAToken;
            return null;
        }
        public static string GetPropertyString(this JToken jThisObject, string sPropertyName, string sDefaultValue = null)
        {
            JToken jPropertyToken = jThisObject.SelectToken(sPropertyName, false);

            if (jPropertyToken != null && jPropertyToken.Type == JTokenType.String)
            {
                return (string)jPropertyToken;
            }
            return sDefaultValue;
        }
        public static decimal? GetPropertyDecimal(this JToken jThisObject, string sPropertyName)
        {
            JToken jPropertyToken = jThisObject.SelectToken(sPropertyName, false);

            if (jPropertyToken != null)
            {
                switch (jPropertyToken.Type)
                {
                    case JTokenType.Float:
                        return (decimal)(double)jPropertyToken;
                    case JTokenType.Integer:
                        return (decimal)(int)jPropertyToken;
                    case JTokenType.String:
                        {
                            decimal decimalOutput = 0;
                            string sTempVal = (string)jPropertyToken;
                            if (decimal.TryParse(sTempVal, out decimalOutput))
                            {
                                return decimalOutput;
                            }
                        }
                        break;
                }
            }
            return null;
        }


        public static T ParsePropertyWithDelegate<T>(this JObject jThisObject, string sPropertyName, Func<JToken, T> ParseDelegate, T defaultValue = default(T))
        {
            JToken jPropertyToken = jThisObject.SelectToken(sPropertyName, false);
            if (jPropertyToken != null)
            {
                return ParseDelegate(jPropertyToken);
            }
            return defaultValue;
        }
        public static List<T> ParsePropertyToListWithDelegate<T>(this JObject jThisObject, string sPropertyName, Func<JToken, T> ParseDelegate, T defaultValue = default(T))
        {
            JToken jPropertyToken = jThisObject.SelectToken(sPropertyName, false);
            List<T> listOutput = new List<T>();
            if (jPropertyToken != null && jPropertyToken.Type == JTokenType.Array)
            {
                JArray jThisArray = (JArray)jPropertyToken;
                foreach (JToken jThis in jThisArray)
                {
                    T tInstance = ParseDelegate(jThis);
                    if (!object.Equals(tInstance, default(T)))
                        listOutput.Add(tInstance);
                }
            }
            return listOutput;
        }
        public static Dictionary<string, T> ParsePropertyToDictionaryWithDelegate<T>(this JObject jThisObject, string sPropertyName, Func<JToken, T> ParseDelegate, T defaultValue = default(T))
        {
            JToken jPropertyToken = jThisObject.SelectToken(sPropertyName, false);
            Dictionary<string, T> dictOutput = new Dictionary<string, T>();
            if (jPropertyToken != null && jPropertyToken.Type == JTokenType.Object)
            {
                JObject jTisObj = (JObject)jPropertyToken;
                foreach (JProperty jProp in jTisObj.Properties())
                {
                    T tInstance = ParseDelegate(jProp.Value);
                    if (!object.Equals(tInstance, default(T)))
                    {
                        dictOutput.Add(jProp.Name, tInstance);
                    }
                }
            }
            return dictOutput;
        }
        public static Dictionary<string, decimal> ParsePropertyToDecimalDictionary(this JObject jThisObject, string sPropertyName)
        {
            JToken jPropertyToken = jThisObject.SelectToken(sPropertyName, false);
            Dictionary<string, decimal> dictOutput = new Dictionary<string, decimal>();
            if (jPropertyToken != null && jPropertyToken.Type == JTokenType.Object)
            {
                JObject jTisObj = (JObject)jPropertyToken;
                foreach (JProperty jProp in jTisObj.Properties())
                {
                    decimal? dValue = null;
                    switch (jProp.Value.Type)
                    {
                        case JTokenType.Float:
                            dValue = (decimal)(double)jProp.Value;
                            break;
                        case JTokenType.Integer:
                            dValue = (decimal)(long)jProp.Value;
                            break;
                        case JTokenType.String:
                            {
                                decimal decimalOutput = 0;
                                string sTempVal = (string)jProp.Value;
                                if (decimal.TryParse(sTempVal, out decimalOutput))
                                {
                                    dValue = decimalOutput;
                                }
                            }
                            break;
                    }
                    if (dValue.HasValue)
                    {
                        dictOutput.Add(jProp.Name, dValue.Value);
                    }
                }
            }
            return dictOutput;
        }
    }
}
